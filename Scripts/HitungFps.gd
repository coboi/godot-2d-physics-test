extends RichTextLabel

func _ready():
	pass

func _physics_process(delta):
	self.set_text(str("FPS ",Engine.get_frames_per_second()))
	pass